# libopenblock
# Copyright © 2022 eyekay
# License: MIT

from sys import platform
import os
import ctypes
import requests
import sys
import shutil
import configparser

if platform == "linux" or platform == "linux2":
    if os.geteuid() != 0:
        print('Please run openblock_cli as root!')
        sys.exit(77) # Permission error according to sysexits.h
    OS_LINUX = True
    HOSTSFILE="/etc/hosts"
    CONFIGDIR="/etc/xdg/openblock/"
elif platform == "darwin":
    print("Mac OS is not supported at this point.")
    sys.exit(1)
elif platform == "win32":
    if ctypes.windll.shell32.IsUserAnAdmin():
        print('Please run openblock_cli as an administrator!')
        sys.exit(5) # ERROR_ACCESS_DENIED
    OS_WIN = True
    HOSTSFILE=" %SystemRoot%\\System32\\drivers\\etc\\hosts"
    CONFIGDIR="%SYSTEMDRIVE%\\ProgramData\\openblock\\"

def internetcheck():
    try:
        requests.get("http://1.1.1.1")
        return 0
    except:
        return 6

def update_cachebank():
    if internetcheck()==6:
        return 6
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    for i in config['blocklists']:
        if os.path.exists(CONFIGDIR+"/cachebank/"+i+'.txt')!=True:
            with open(CONFIGDIR+"/cachebank/"+i+".txt",'w') as j:
                content = requests.get(config['blocklists'][i])
                j.write(content.text)
    for i in config['allowlists']:
        if os.path.exists(CONFIGDIR+"/cachebank/"+i+'.txt')==True:
            with open(CONFIGDIR+"/cachebank/"+i+".allow.txt",'w') as j:
                content = requests.get(config['allowlists'][i])
                j.write(content.text)

def update_cachebank_force():
    if internetcheck()==6:
        return 6
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    for i in config['blocklists']:
        with open(CONFIGDIR+"/cachebank/"+i+".txt",'w') as j:
            content = requests.get(config['blocklists'][i])
            j.write(content.text)
    for i in config['allowlists']:
        with open(CONFIGDIR+"/cachebank/"+i+".allow.txt",'w') as j:
            content = requests.get(config['allowlists'][i])
            j.write(content.text)


def init():
    if os.path.exists(CONFIGDIR+'init.lock')==True:
        return 8
    if OS_LINUX:
        os.mkdir(CONFIGDIR)
        os.mkdir(CONFIGDIR+"/cachebank/")
    elif OS_WIN:
        os.mkdir(CONFIGDIR)
        os.mkdir(CONFIGDIR+"\\cachebank\\")

    if internetcheck()==6:
        return 6

    shutil.move(HOSTSFILE,HOSTSFILE+".orig")

    config = configparser.ConfigParser()
    config['blocklists']={}
    config['blocklists']['AdAway_hosts']='https://adaway.org/hosts.txt'
    config['blocklists']['Pete_Lowe_blocklist']='https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext'
    config['blocklists']['StevenBlack_Unified_hosts']='https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts'
    config['blocklists']['someonewhocares']='http://someonewhocares.org/hosts/hosts'
    config['blocklists']['mvps']='https://winhelp2002.mvps.org/hosts.txt'
    config['allowlists']={}
    config['blocked_domains']={}
    config['allowed_domains']={}
    config['redirects']={}
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

    update_cachebank()

    temp_hostsfile=''
    for i in os.listdir(CONFIGDIR+"/cachebank/"):
        with open(CONFIGDIR+'/cachebank/'+i,'r') as j:
            line = j.readline()
            while line != '':
                if line.startswith("#"):
                    pass
                else:
                    temp_hostsfile+=line
                line = j.readline()
    
    with open(HOSTSFILE,'w') as j:
        j.write(temp_hostsfile)    
    with open(CONFIGDIR+'openblock.lock', 'w') as j:
        j.write('The presence of this file indicates that OpenBlock is running (i.e., the hosts file is in a modified state.)')
    with open(CONFIGDIR+'init.lock', 'w') as j:
        j.write('The presence of this file indicates that OpenBlock has been set up.')

def stop():
    if os.path.exists(CONFIGDIR+'openblock.lock')!=True:
        return 8
    os.remove(HOSTSFILE)
    shutil.copy(HOSTSFILE+".orig", HOSTSFILE)
    os.remove(CONFIGDIR+'openblock.lock')

def block(hostname):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    for i in config['allowed_domains']:
        if i == hostname:
            config['allowed_domains'].pop(hostname)
    config['blocked_domains'][hostname]='E' # I needed to fill in some value as its a dictionary
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def unblock(hostname):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    for i in config['blocked_domains']:
        if i == hostname:
            try:
                config['blocked_domains'].pop(hostname)
            except:
                return 7
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def allow(hostname):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    for i in config['blocked_domains']:
        if i == hostname:
            config['blocked_domains'].pop(i)
    config['allowed_domains'][hostname]='E'
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def unallow(hostname):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    for i in config['allowed_domains']:
        if i == hostname:
            try:
                config['allowed_domains'].pop(i)
            except:
                return 7
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def blocklist_rm(blocklist):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    for i in config['blocklists']:
        if i == blocklist:
            config['blocklists'].pop(i)
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def blocklist_add(blocklist,url):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    config['blocklists'][blocklist]=url
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def allowlist_add(allowlist,url):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    config['allowlists'][allowlist]=url
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def allowlist_rm(blocklist):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    for i in config['allowlists']:
        if i == blocklist:
            config['allowlists'].pop(i)
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def redirect(url,ip):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    config['redirects'][url]=ip
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def unredirect(url):
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")
    for i in config['redirects']:
        if i == url:
            config['redirects'].pop(url)
    with open(CONFIGDIR+"openblock.conf", 'w') as configfile:
        config.write(configfile)

def start():
    if os.path.exists(CONFIGDIR+'openblock.lock')==True:
        return 8
    update_cachebank()
    temp_hostsfile=[]
    redirects={}
    for i in os.listdir(CONFIGDIR+"/cachebank/"):
        if i.endswith(".allow.txt"):
            pass
        else:
            with open(CONFIGDIR+'/cachebank/'+i,'r') as j:
                line = j.readline()
                while line != '':
                    if line.startswith("#"):
                        pass
                    else:
                        domain=''
                        if line.startswith("0.0.0.0"):
                            domain=line.lstrip("0.0.0.0 ")
                        if line.startswith("127.0.0.1"):
                            domain=line.lstrip("127.0.0.1 ")
                        temp_hostsfile+=[domain]
                    line = j.readline()
    for i in os.listdir(CONFIGDIR+"/cachebank/"):
        if i.endswith(".allow.txt"):
            with open(CONFIGDIR+'/cachebank/'+i,'r') as j:
                line = j.readline()
                while line != '':
                    if line.startswith("#"):
                        pass
                    else:
                        domain=''
                        if line.startswith("0.0.0.0"):
                            domain=line.lstrip("0.0.0.0 ")
                        if line.startswith("127.0.0.1"):
                            domain=line.lstrip("127.0.0.1 ")
                        try:
                            temp_hostsfile.remove(domain)
                        except:
                            pass
                    line = j.readline()
        else:
            pass
    
    config=configparser.ConfigParser()
    config.read(CONFIGDIR+"openblock.conf")

    for i in config['allowed_domains']:
        try:
            temp_hostsfile.remove(i)
        except:
            pass
    
    for i in config['blocked_domains']:
        temp_hostsfile+=[i]
    
    for i in config['redirects']:
        try:
            temp_hostsfile.remove(i)
        except:
            pass
        redirects[i]=config['redirects'][i]
    shutil.move(HOSTSFILE,HOSTSFILE+".orig")
    with open(HOSTSFILE,'w') as j:
        for i in temp_hostsfile:
            j.write("127.0.0.1 "+i)
        for i in redirects:
            j.write(redirects[i]+' '+i)
    with open(CONFIGDIR+'openblock.lock', 'w') as j:
        j.write('The presence of this file indicates that OpenBlock is running (i.e., the hosts file is in a modified state.)')