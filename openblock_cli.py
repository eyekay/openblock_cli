# openblock_cli
# Copyright © 2022 eyekay
# License: MIT

import libopenblock
from sys import platform
import sys
import os

if platform == "linux" or platform == "linux2":
    if os.geteuid() != 0:
        print('Please run openblock_cli as root!')
        sys.exit(77) # Permission error according to sysexits.h
    CONFIGDIR="/etc/xdg/openblock/"
elif platform == "darwin":
    print("Mac OS is not supported at this point.")
    sys.exit(1)
elif platform == "win32":
    if ctypes.windll.shell32.IsUserAnAdmin():
        print('Please run openblock_cli as an administrator!')
        sys.exit(5) # ERROR_ACCESS_DENIED
    CONFIGDIR="%SYSTEMDRIVE%\\ProgramData\\openblock\\"

arg=sys.argv[1]
if arg=='init':
    j=libopenblock.init()
    if j==8:
        print("Ahoy mate, OpenBlock is already done set up")
    else:
        print('OpenBlock has been set up.\nOpenBlock is now running.')
elif arg=='stop':
    j=libopenblock.stop()
    if j==8:
        print("OpenBlock ain't runnin\' anyway, matey")
    else:
        print('OpenBlock is no longer running.')
elif arg=='start':
    j=libopenblock.start()
    if j==8:
        print("OpenBlock is already runnin\', mate")
    else:
        print('OpenBlock is now running.')
elif arg=='block':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer domain name empty, mate')
    else:
        libopenblock.block(sys.argv[2])
        print(sys.argv[2]+' has now been blocked. Your changes will take effect the next time OpenBlock is started.')
elif arg=='allow':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer domain name empty, mate')
    else:
        libopenblock.allow(sys.argv[2])
        print(sys.argv[2]+' has now been allowed. Your changes will take effect the next time OpenBlock is started.')
elif arg=='unblock':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer domain name empty, mate')
    else:
        h=libopenblock.unblock(sys.argv[2])
        if h!=7:
            print(sys.argv[2]+' has now been blocked. Your changes will take effect the next time OpenBlock is started.')
        else:
            print(sys.argv[2]+' ain\'t blocked already, matey, and don\'t ye go fixing around things that ain\'t broken')
elif arg=='unallow':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer domain name empty, mate')
    else:
        h=libopenblock.unallow(sys.argv[2])
        if h!=7:
            print(sys.argv[2]+' has now been allowed. Your changes will take effect the next time OpenBlock is started.')
        else:
            print(sys.argv[2]+' ain\'t allowed already, matey, and don\'t ye go fixing around things that ain\'t broken')
elif arg=='redirect':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer domain name empty, mate')
    try:
        h=sys.argv[3]
    except:
        print('Ye can\'t have yer IP Address empty, mate')
    else:
        libopenblock.redirect(sys.argv[2], sys.argv[3])
        print(sys.argv[2]+' will now redirect to '+sys.argv[3]+'.')
elif arg=='unredirect':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer domain name empty, mate')
    else:
        libopenblock.unredirect(sys.argv[2])
elif arg=='blocklist-add':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer blocklist name empty, mate')
    try:
        h=sys.argv[3]
    except:
        print('Ye can\'t have yer blocklist url empty, mate')
    else:
        libopenblock.blocklist_add(sys.argv[2], sys.argv[3])
        print(sys.argv[2]+', located at '+sys.argv[3]+' has been added as a blocklist.')
elif arg=='allowlist-add':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer allowlist name empty, mate')
    try:
        h=sys.argv[3]
    except:
        print('Ye can\'t have yer allowlist url empty, mate')
    else:
        libopenblock.blocklist_add(sys.argv[2], sys.argv[3])
        print(sys.argv[2]+', located at '+sys.argv[3]+' has been added as an allowlist.')
elif arg=='blocklist-rm':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer blocklist name empty, mate')
    else:
        libopenblock.blocklist_rm(sys.argv[2])
elif arg=='allowlist-rm':
    try:
        h=sys.argv[2]
    except:
        print('Ye can\'t have yer allowlist name empty, mate')
    else:
        libopenblock.allowlist_rm(sys.argv[2])
elif arg=='update':
    libopenblock.update_cachebank_force()
    print('Blocklist and allowlist caches have now been updated.')
else:
    print('openblock_cli [option] [arg1] [arg2]')
    print('OpenBlock is a simple, system-wide ad blocker.\nPossible commands:')
    print('init\tSet up and start OpenBlock\nstart\nstop\nblock [domain]\nunblock [domain]\nallow [domain]\nunallow [domain]')
    print('blocklist-add [url]\nallowlist-add [url]\nblocklist-rm [name]\tRemove a blocklist\nallowlist-rm [name]\tRemove a allowlist\nupdate\t Update cache')