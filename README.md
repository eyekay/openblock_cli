# OpenBlock/ openblock_cli/ libopenblock Documentation

OpenBlock is a simple, system-wide ad blocker. It consists of a GUI (OpenBlock) and a CLI (openblock_cli). Both are based on a common backend (libopenblock)

This page documents the various commands in openblock_cli, and the internal workings of libopenblock.

It is assumed no other software or user will be touching the hosts file while libopenblock is installed.

OpenBlock does *NOT* tamper with hosts.allow and hosts.deny. Allowing and blocking domains are managed by writing or not writing to the main hosts file.

Blocklists
==========
By default, the blocklists [AdAway](https://adaway.org/hosts.txt), [Yoyo.org](https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext), [StevenBlack Unified](https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts), [Someonewhocares](http://someonewhocares.org/hosts/hosts) and [Winhelp2002 MVPS](https://winhelp2002.mvps.org/hosts.txt) are configured to be used. However, none of the above are shipped with OpenBlock and they are downloaded at first run/ openblock_cli init.

Update Mechanism
================
openblock_cli update can be run every other day using cron (or its Windows equivalent).

The cron job should call openblock_cli stop, openblock_cli update and then openblock_cli start.

openblock_cli should NOT be run at each boot, or each startup of openblock{cli|gui} as changes to the source hostsfiles only happen every few days or so.

Priority
========
Blocklists < Allowlists < Allowed Domains < Blocked domains < Domain Redirects

Error codes
===========

0: :)

1: :( (Unexplainable error)

5: Not admin on Windows

6: :| (No internet)

7: Parsing Error

77: Not admin on Linux (and maybe Mac)

8: Nothing to do

Commands
========

openblock_cli init

	At trigger, HOSTSFILE is at default state.	
	
	Check for internet
	Backup previous hosts file to HOSTSFILE.orig
	Update cachebank
	Compile default blocklists
	Add default blocklists to config:blocklists
	Add compiled blocklist to HOSTSFILE
	Create /etc/xdg/openblock/openblock.lock

openblock_cli stop

	At trigger, HOSTSFILE is at edited state.

	HOSTSFILE (edited) is removed
	HOSTSFILE.orig is copied to HOSTSFILE
	Remove /etc/openblock.lock
	
openblock_cli start

	At trigger, HOSTSFILE is at default state.
	
	Update cachebank
	In temp_hostsfile (a list of domains):
		Add blocklist domains
		Remove allowlist domains
	Check for blocked domains from config:blocked_domains
	Check for allowed domains from config:allowed_domains
	In temp_hostsfile:
		Remove allowed domains
		Add blocked domains
	Check for domain redirects from config:redirects
	In temp_hostsfile:
		Remove edirected domain already added by blocklist/blocked domains
	HOSTSFILE.orig already exists, but move HOSTSFILE to HOSTSFILE.orig for redundancy
	Add temp_hostsfile to HOSTSFILE
	Add redirects
	Create /etc/openblock.lock
	
openblock_cli block [hostname]

	If [hostname] is in config:allowed_domains, remove it from there
	Add [hostname] to config:blocked_domains (in case it is in an allowlist)
	If openblock is running (/etc/openblock.lock exists),
		Inform user that the current HOSTSFILE won't be edited and their changes will be reflected after stopping and starting openblock again

openblock_cli unblock [hostname]

	If [hostname] is in config:blocked_domains, remove it from there

openblock_cli unallow [hostname]

	If [hostname] is in config:allowed_domains, remove it from there

openblock_cli allow [hostname]

	If [hostname] is in config:blocked_domains, remove it from there
	Add [hostname] to config:allowed_domains (in case it is in a blocklist)
	If openblock is running (/etc/openblock.lock exists),
		Inform user that the current HOSTSFILE won't be edited and their changes will be reflected after stopping and starting openblock again

openblock_cli redirect [hostname] [IP Address]

	Add [hostname],[IP Address] to config:redirects

openblock_cli unredirect [hostname]

	Remove [hostname],[IP Address] from config:redirects

openblock_cli blocklist-add [name] [url]

	Add [url] to config:blocklists

openblock_cli allowlist-add [name] [url]

	Add [url] to config:allowlists

openblock_cli blocklist-rm [name]

	Remove [url] from config:blocklists

openblock_cli allowlist-rm [name]

	Remove [url] form config:allowlists
	
openblock_cli update {Update cachebank}

	Check for internet
	Update all blocklists from URLs
	Update cachebank/blocklistname.txt
	Update all allowlists from URLs
	Update cachebank/allowlistname.txt